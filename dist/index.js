'use strict'
const __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
  function adopt (value) { return value instanceof P ? value : new P(function (resolve) { resolve(value) }) }
  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled (value) { try { step(generator.next(value)) } catch (e) { reject(e) } }
    function rejected (value) { try { step(generator.throw(value)) } catch (e) { reject(e) } }
    function step (result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected) }
    step((generator = generator.apply(thisArg, _arguments || [])).next())
  })
}
const __importDefault = (this && this.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { default: mod }
}
Object.defineProperty(exports, '__esModule', { value: true })
const express_1 = __importDefault(require('express'))
// import express = require('express');
const cors_1 = __importDefault(require('cors'))
const mongoose_1 = __importDefault(require('mongoose'))
const uuid_1 = require('uuid')
const app = express_1.default()
const port = 3000
const uri = 'mongodb+srv://Akiko:x75p0kdzdIsbVu0Q@cluster0.l9jaw.mongodb.net/todolist?retryWrites=true&w=majority'
mongoose_1.default
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('connected to DB')
  })
  .catch((err) => console.log(err))
const ListSchema = new mongoose_1.default.Schema({
  title: String,
  completed: {
    type: Boolean,
    default: false
  },
  url: {
    type: String,
    default: ''
  },
  order: Number
})
const List = mongoose_1.default.model('lists', ListSchema)
app.use(cors_1.default())
app.use(express_1.default.json())
app.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function * () {
  const posts = yield List.find()
  res.send(posts)
}))
app.get('/:url', (req, res) => __awaiter(void 0, void 0, void 0, function * () {
  const { url } = req.params
  const post = yield List.findOne({
    url: `http://9f40e1722315.ngrok.io/${url}`
  })
  res.send(post)
}))
app.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function * () {
  const { title, order } = req.body
  const post = new List({
    title: title,
    url: `http://9f40e1722315.ngrok.io/${uuid_1.v4()}`,
    order
  })
  const result = yield post.save()
  res.send(result)
}))
app.delete('/', (req, res) => __awaiter(void 0, void 0, void 0, function * () {
  const result = yield List.deleteMany({})
  res.send(result)
}))
app.patch('/:url', (req, res) => __awaiter(void 0, void 0, void 0, function * () {
  const { title, order } = req.body
  const { url } = req.params
  const result = yield List.findOneAndUpdate({
    url: `http://9f40e1722315.ngrok.io/${url}`
  }, {
    $set: {
      title,
      completed: true,
      order
    }
  }, {
    new: true
  })
  res.send(result)
}))
app.delete('/:url', (req, res) => __awaiter(void 0, void 0, void 0, function * () {
  const { url } = req.params
  const result = yield List.findOneAndDelete({
    url: `http://9f40e1722315.ngrok.io/${url}`
  })
  res.send(result)
}))
app.listen(port, () => {
  console.log(`Example app listening at ${port}`)
})
