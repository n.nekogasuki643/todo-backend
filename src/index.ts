import express, { Request, Response, Application } from 'express'
// import express = require('express');

import cors from 'cors'
import mongoose from 'mongoose'
import { v4 as uuidv4 } from 'uuid'

const app: Application = express()

const port = 3000
const uri =
    'mongodb+srv://Akiko:x75p0kdzdIsbVu0Q@cluster0.l9jaw.mongodb.net/todolist?retryWrites=true&w=majority'

mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('connected to DB')
  })
  .catch((err) => console.log(err))

const ListSchema = new mongoose.Schema({
  title: String,
  completed: {
    type: Boolean,
    default: false
  },
  url: {
    type: String,
    default: ''
  },
  order: Number
})

const List = mongoose.model('lists', ListSchema)

app.use(cors())
app.use(express.json())

app.get('/', async (req: Request, res: Response) => {
  const posts = await List.find()
  res.send(posts)
})

app.get('/:url', async (req: Request, res: Response) => {
  const { url } = req.params
  const post = await List.findOne({
    url: `http://9f40e1722315.ngrok.io/${url}`
  })
  res.send(post)
})

app.post('/', async (req: Request, res: Response) => {
  const { title, order } = req.body
  const post = new List({
    title: title,
    url: `http://9f40e1722315.ngrok.io/${uuidv4()}`,
    order
  })
  const result = await post.save()
  res.send(result)
})

app.delete('/', async (req: Request, res: Response) => {
  const result = await List.deleteMany({})
  res.send(result)
})

app.patch('/:url', async (req: Request, res: Response) => {
  const { title, order } = req.body
  const { url } = req.params

  const result = await List.findOneAndUpdate(
    {
      url: `http://9f40e1722315.ngrok.io/${url}`
    },
    {
      $set: {
        title,
        completed: true,
        order
      }
    },
    {
      new: true
    }
  )
  res.send(result)
})

app.delete('/:url', async (req: Request, res: Response) => {
  const { url } = req.params
  const result = await List.findOneAndDelete({
    url: `http://9f40e1722315.ngrok.io/${url}`
  })
  res.send(result)
})

app.listen(port, () => {
  console.log(`Example app listening at ${port}`)
})
